<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>
		  <footer>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="col-11 sign-up-mob">
                        <p>Sign up and get exclusive
                            offers and more!</p>
                        <form role="search" class="form-inline">
                            <input type="text" placeholder="Email">
                            <button type="submit">Go</button>
                        </form>
                    </div>
                </div>
                <div class="col-xl-9 col-lg-10 col-md-10 col-11 footer-start">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12 learning pl-md-0">
                            <h4>shop with us</h4>
                            <ul>
                                <li><a href="#">Weekly lenses</a></li>
                                <li> <a href="#">monthly lenses</a></li>
                                <li><a href="#"> daily lenses</a></li>
                                <li><a href="#"> glasses</a></li>
                                <li><a href="#"> accessories</a></li>
                                <li><a href="#"> all brands</a></li>
                            </ul>

                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12 learning">
                            <h4>help</h4>
                            <ul>
                                <li><a href="#">contact us</a></li>
                                <li> <a href="#">my account</a></li>
                                <li><a href="#">faq</a></li>
                                <li><a href="#"> blog</a></li>
                                <li><a href="#">face shape guide</a></li>
                                <li> <a href="#">autorefill</a></li>
                                <li><a href="#">sownload pd ruler</a></li>
                                <li><a href="#">fsa / hsa information</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-6 pt-lg-0 pt-md-4 pt-sm-4 col-sm-6 col-12 learning">
                            <h4>About us</h4>
                            <ul>
                                <li><a href="#">the lensdirect story</a></li>
                                <li> <a href="#">terms and conditions</a></li>
                                <li><a href="#"> privacy policy</a></li>
                                <li><a href="#">affiliate program</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12 learning">
                            <h4>follow lensdirect</h4>
                            <ul class="social-icons">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                            </ul>
                            <h4>secure payment</h4>
                            <ul class="payment">
                                <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/visa.png"></a></li>
                                <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/master.png"></a></li>
                                <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/amex.png"></a></li>
                                <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/Discover-logo.png"></a></li>
                                <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/Paypal-logo.png"></a></li>
                            </ul>

                        </div>
                    </div>
                </div>
                <div class="col-11">
                    <div class="logo-footer wow zoomIn" data-wow-delay="0s">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/lens-logo_03.png">
                    </div>
                </div>
                <div class="col-11 material wow fadeIn" data-wow-delay="0s">
                    <p>The material provided on this site is for informational purposes only. Please make sure you
                        get your eyes examined regularly and always follow your eye care professional's instructions
                        for the proper use and care of
                        your contact lenses. It's important to note that if you experience any pain or discomfort
                        from your contact lens, discontinue use immediately and consult your eye care professional.
                        All discounts and promotions are applied
                        to future purchases only and cannot be applied to past purchases. © LensDirect.com All
                        Rights Reserved</p>
                </div>
                <div class="col-12 icons-social">
                    <div class="row w-100">
                        <div class="col-4 faq-icons">
                            <div class="img-icon wow fadeIn" data-wow-delay="0.2">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/question-mark_.png" alt="question-mark">
                            </div>
                            <small class="wow fadeIn" data-wow-delay="0.4s">faq</small>
                        </div>
                        <div class="col-4 faq-icons">
                            <div class="img-icon wow fadeIn" data-wow-delay="0.4s">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/phone_03.png" alt="phone"></div>
                            <small class="wow fadeIn" data-wow-delay="0.6s">phone</small>
                        </div>
                        <div class="col-4 faq-icons">
                            <div class="img-icon wow fadeIn" data-wow-delay="0.6s">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/email_03.png" alt="email">
                            </div>
                            <small class="wow fadeIn" data-wow-delay="0.8s">email</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

		<?php wp_footer(); ?>

	</body>
</html>
