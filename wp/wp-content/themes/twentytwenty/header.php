<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?><!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >

		<link rel="profile" href="https://gmpg.org/xfn/11">
		<meta charset="UTF-8" />
       <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <!-- stylesheet -->
    <link rel="stylesheet" href="<?php ?>https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php ?>https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
    <!--    <link rel="stylesheet" href="assets/css/style.css">-->
    <!-- <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/swiper.min.css"> -->
    <link rel="stylesheet" href="<?php ?>https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
		<?php wp_head(); ?>

	</head>

	<body <?php body_class(); ?>>
 <!-- top-bar -->
 <div id="top-bar">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="time-offer">
                        <p>Limited time offer - save 30% on Lens replacement</p>
                        <span> <a>code: replace30</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- top-bar mob -->
    <div class="mob-top-bar">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="time-offer">
                        <p>20% oFF on all orders </p>
                        <span> <a>code: save20</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <header>
        <div class="container-fluid">
            <div class="flex-box">
                <!-- mobile view -->
                <div class="toggle">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarresponsive" aria-controls="navbarresponsive" aria-expanded="false"
                            aria-label="Toggle navigation">
                            <span>&#9776;</span>
                        </button>
                        <div class="collapse navbar-collapse custom-collapse" id="navbarresponsive">
                            <div class="close-menu"></div>
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item dropdown">
                                    <a class="dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Contact Lenses
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                        <a class="dropdown-item" href="#">Acuvue</a>
                                        <a class="dropdown-item" href="#">Air Optix</a>
                                        <a class="dropdown-item" href="#">Biofinity</a>
                                        <a class="dropdown-item" href="#">Dailies</a>
                                        <a class="dropdown-item" href="#">Baush & Lomb</a>
                                        <a class="dropdown-item" href="#">View all brands</a>
                                    </div>
                                </li>
                                <li class="">
                                    <a class="nav-link" href="#">Eyeglasses</a>
                                </li>
                                <li class="">
                                    <a class="nav-link" href="#">Sunglasses</a>
                                </li>
                                <li class="">
                                    <a class="nav-link" href="#">Lens Replacement <span>NEW</span></a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <!-- logo -->
                <div class="brand-logo">
                    <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/LensDirect.png" alt="LensDirect-log"></a>
                </div>
                <!-- nav-bar -->
                <div class="nav">
                    <ul class="nav-ul">
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                Contact Lenses
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="#">Acuvue</a>
                                <a class="dropdown-item" href="#">Air Optix</a>
                                <a class="dropdown-item" href="#">Biofinity</a>
                                <a class="dropdown-item" href="#">Dailies</a>
                                <a class="dropdown-item" href="#">Baush & Lomb</a>
                                <a class="dropdown-item" href="#">View all brands</a>
                            </div>
                        </li>
                        <li class="">
                            <a class="nav-link" href="#">Eyeglasses</a>
                        </li>
                        <li class="">
                            <a class="nav-link" href="#">Sunglasses</a>
                        </li>
                        <li class="">
                            <a class="nav-link" href="#">Lens Replacement <span>NEW</span></a>
                        </li>
                    </ul>
                </div>
                <!-- search -->
                <div class="navbar-text-icons">
                    <ul>
                        <li><a href="#"></a>
                            <div id="search">
                                <i class="fas fa-search" id="search-icon"></i>
                            </div>
                            </a>
                        </li>
                        <li> <a href="">login</a></li>
                        <li> <a href="">help</a></li>
                        <li><a href="#"><span class="price"><i class="fa fa-shopping-cart"><span>2</span></i>
                                </span></a></li>
                    </ul>
                </div>
            </div>
            <div class="search-div">
                <input type="text" placeholder="Type here to search...">
            </div>
        </div>
    </header>
		<?php

