<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'lens-direct' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Phh~#=IX?<1 1N?:-&JgLAig<>1ZuN$rh2r|ep~D-k;~Cy(koD^!P=/@^{X7]]5@' );
define( 'SECURE_AUTH_KEY',  'V~)2wl_$.@2!T|I1st.XZf8qbi9~dzK{EduQ4yO56+N CeT/eQ]V=Z4:#pe@__2A' );
define( 'LOGGED_IN_KEY',    '90CN&;8[-!3y$K{rUTh+=_p[)<YumTr(a`M@{[Qb#g%J*1x)<3^52e#XS>_z%Zy=' );
define( 'NONCE_KEY',        'c$jF&VufG,qzL;u0$<T^;g9^+}sZJk!z!76,XCfU=Kr,1Ojp(&>#mml:zFnL0T`a' );
define( 'AUTH_SALT',        '2eb.O0_+B6;*4&qPQl,o${$R9<Tv$TX[BAVsNrn~Psng.QESbe-{-Cwdrv{&LBF4' );
define( 'SECURE_AUTH_SALT', '(QAky^>i2[M_^zNNcdm-K5E4sf!NdPRr5=[V%=%nty@NC1V~:9,|oLKYbIRj-j;#' );
define( 'LOGGED_IN_SALT',   'w|Oi>EO 8RV!i2d-^D%aXy0C$nVtK2v &+MR3PUsfYUqX@2Z*LF_e=eSzN[wS&E.' );
define( 'NONCE_SALT',       ' GZ&f[|Aau(_OXgOqReW;h^Y%zVM8iCS{4I`V-%xThc.. 0_50%;dG*z,ooC|Y~-' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
