jQuery(document).ready(function () {
	
  "use strict";
 
  var swiper = new Swiper('.swiper-container', {
    // slidesPerView: 1,
    spaceBetween: 10,
    // init: false,
    loop:true,
    // effect: 'fade',
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    autoplay: 
    {
      delay: 2000,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      0: {
        slidesPerView: 1,
        spaceBetween: 5,
        },
      640: {
        slidesPerView: 1,
        spaceBetween: 10,
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 20,
      },
      991: {
        slidesPerView: 3,
        spaceBetween: 20,
      },
      1200: {
        slidesPerView: 3,
        spaceBetween: 25,
      },
      1400: {
        slidesPerView: 4,
        spaceBetween: 25,
      },
    }
  });

  // Custom Navbar Collapse Menu on Mobile
  jQuery(".custom-collapse .close-menu").on("click", function() {
    if (jQuery(".custom-collapse").hasClass("show")) {
      jQuery(".custom-collapse").addClass("closed");
      setTimeout(function() {
        jQuery(".custom-collapse").removeClass("closed");
        jQuery(".custom-collapse").removeClass("show");
      }, 300);
    }
  });
  ////////Search-bar////////
  jQuery("#search").on("click", function() {
    if (jQuery(".search-div").hasClass("open")) {
      jQuery(".search-div").removeClass("open");
    } else {
      jQuery(".search-div").addClass("open");
    }
  });
 ///////////animateCSS
 new WOW().init();
});